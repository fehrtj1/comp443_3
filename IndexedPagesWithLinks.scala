// Look through pages and assign weights based on number of crossed links

import scala.collection.mutable

class IndexedPagesWithLinks extends IndexedPages{

  private val d = 0.85 // damping factor
  private val n = length //

  override def getWeights: Seq[Double] = {
    val scores = getScoreMap
    for( page <- pages if(page._2.links.size > 0) ){
      var pgSize = (1.0 / page._2.links.size)
      for (link <- page._2.links){
        if(scores.contains(link)) scores(link) += pgSize
      }
    }
    for(score <- scores) scores(score._1) = applyDamping(score._2)
  }

  // returns a clean Map to assign page scores
  private def getScoreMap: mutable.Map[String, Double] = {
    for(page <- getItems) yield (page.url, 0.0)
  }

  // Applies damping factor to the score retrieved from the page
  private def applyDamping(pg: Double): Double = {
    ((1 - d) / n) + d * pg)
  }
}
