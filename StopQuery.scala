class StopQuery extends Query {
    override def getWeights: Seq[Double] =  for( word <- words ) yield ( if( stopList.words.contains(word) ) 3.5 else 1.0 )
}
