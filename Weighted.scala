trait Weighted[A] {

    def getItems: Seq[A]
    def getWeights: Seq[Double]

    def sumIf(f: A => Boolean): Double = {
        (getItems zip getWeights).foldRight(0.0){ (x, sum) => if(f(x._1)) sum + x._2 else sum }
    }
}
