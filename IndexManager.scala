
import akka.actor.Actor
import akka.actor.Props
import akka.actor.ActorRef
import scala.collection.mutable
import java.io._
import scala.io._

case class StartCrawling(nFetchers: Int, indexPagesType: Int = IndexedPageType.default)

object IndexedPageType {
	val default = 0
	val shortUrls = 1
	val urlLinking = 2
}

class IndexManager extends Actor{

	private var workers: scala.collection.mutable.Vector[ActorRef] = Nil
	private var waitingWorkers = new scala.collection.mutable.Queue[ActorRef]
	private val pageQueue = new scala.collection.mutable.Queue[String]
	private val indexedPages: IndexedPages = Nil

	private def createFetcherWorkers(numWorkers: Int) = {
		for(i <- 0 until numWorkers) yield
			context.actorOf(Props[Fetcher], name =s"worker-${i}")
	}

	private def assignTask(worker: ActorRef) = {
		// Give worker new url
		if(pageQueue.size > 0){
			worker ! IndexRequest(pageQueue.dequeue)

			// Check if any workers are waiting
			while(pageQueue.size > 0 && waitingWorkers.size >0){
				waitingWorkers.dequeue ! IndexRequest(pageQueue.dequeue)
			}
		}else{
			// Add worker to queue if there are no urls in the queue
			waitingWorkers.enqueue(worker)
		}
	}

	private def initializeIndexedPages(t: Int) = {
		t match {
			case IndexedPageType.default => indexedPages = new IndexedPages
			case IndexedPageType.shortUrls => indexedPages = new IndexedPageWithUrls
			case IndexedPageType.urlLinking => indexedPages = new IndexedPageWithLinks
			case _ => indexedPages = new IndexedPages
		}
	}

	def receive = {
		case q: Query => {
			if(q.words == Nil)
				context.system.terminate
			else
				sender ! indexedPages.search(q)
		}
		case StartCrawling(n, t) => {
			initializeIndexedPages(t)
			for(u <- TopFiftyUrls.urls) pageQueue.enqueue(u)
			workers = createrFetcherWorkers(n)
			for(w <- workers) w ! IndexRequest(pageQueue.dequeue)
		}
		case p: Option[Page] => {
			p match {
				case Some(pg) => {
					// Add page to
					indexedPages.add(pg)

					// Add links to queue
					pg.links.foreach((l) => if(!indexedPages.contains(l)) pageQueue.enqueue(l))
					
					assignTask(sender)
				}
				case None => assignTask(sender)
			}
		}
	}
}
