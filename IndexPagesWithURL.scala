// Assigns weights according to the length of the URL,
// i.e. shorter URLs recieve higher weights

class IndexedPagesWithURLs extends IndexedPages{

  private val d = 0.5

  override def getWeights: Seq[Double] = {
    for(page <- getItems) yield ( getUrlScore(page.url) )
  }

  def getUrlScore(url: String): Double = {
    val newUrl = url.replace("http://").replace("https://")
    val index = url.indexOf("/") + 1
    val len = newUrl.length
    (d * index + len - index) / len
  }
}
