class Query(val words: Seq[String]) extends Weighted[String]{
    def getItems: Seq[String] = {
        words
    }
    def getWeights: Seq[Double] =  {
        for(i<-1 to getItems.length) yield 1.0
    }
}
