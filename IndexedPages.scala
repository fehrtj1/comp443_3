import scala.collection.mutable

class IndexedPages extends Seq[Page] with Weighted[Page]{

	private var pages = mutable.Map[String, Page]()
	private val pageDampening = 0.3

	def iterator: Iterator[Page] = pages.values.iterator
	def length: Int = pages.size
	def apply(idx: Int):Page = getItems(idx)

	def getItems: Seq[Page] = pages.values.toSeq
	def getWeights: Seq[Double] = for(i <- 1 to length) yield 1.0

	def add(p: Page) = if(!pages.contains(p.url)) pages(p.url) = p
	def contains(url: String):Boolean = pages.contains(url)

	def search(q: Query): SearchResults = {

		val nIndexedPages = length
		val urls = ( for(p <- pages) yield p._1 ).toSeq
		val queryAttr = q.getItems.zip(q.getWeights)

		val weights = (for(x <- getItems.zip(getWeights)) yield (queryAttr.foldLeft(0.0) {
			(score, q) => {
				val qUpper = q._1.toUpperCase
				val count = x._1.text.split("\\W+").foldLeft(0){
					(ct,w) => {ct + (if(qUpper == w.toUpperCase) 1 else 0)}
				}
				score + count * (q._2 + pageDampening*x._2)
			} 
		})).toSeq

		new SearchResults(q, nIndexedPages, urls, weights)
	}
}
