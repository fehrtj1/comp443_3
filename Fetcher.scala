case class IndexRequest(url: String)

class Fetcher extends Actor {

  def receive = {
    case IndexRequest(url: String) => {
        sender ! Page.fetchPage(url)
    }
}
