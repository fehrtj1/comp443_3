
object MainNoActors {
  def main(args: Array[String]) = {
    val index = new IndexedPages()

    addTop50Pages(index)

    val queries = Vector(
                          Vector("news"),
                          Vector("apple"),
                          Vector("sports", "ncaa"),
                          Vector("watch", "movies")
                        ).map{ new Query(_) }

    for(q <- queries) {
      val results = index.search(q)
      println(q)
      results.top(8).foreach{ case (url, score) => printf("%10.4f   %s\n", score, url) }
      println("")
    }
  }

  def addTop50Pages(index: IndexedPages) = {

    val pagesToAdd = TopFiftyUrls.urls.flatMap{ (u: String) => Page.fetchPage(u) }

    // uncomment to see the content of the pages
    for(p <- pagesToAdd) {println(p.url); println(p.text); println("\n\n")}

    for(p <- pagesToAdd) index.add(p)
  }
}
