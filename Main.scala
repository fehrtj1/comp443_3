object Main {

  val numFetchers = 10

  def main(args: Array[String]): Unit = {
    val system = ActorSystem("search-engine-system")
    val indexManager = system.actorOf(Props[IndexManager], name = "IndexManager")
    val prompter = system.actorOf(Props[p\Prompter], name = "Prompter")
	
	// Change the IndexedPageType so that IndexManager uses the other subclasses of IndexedPages
    indexManager ! StartCrawling(numFetchers, IndexedPageType.shortUrls)
    prompter ! StartPrompting
  }

}
